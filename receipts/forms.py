from django.forms import ModelForm
from receipts import models

class ReceiptForm(ModelForm):
    class Meta:
        model = models.Receipt
        fields = [
            'vendor',
            'total',
            'tax',
            'date',
            'category',
            'account',
        ]

class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = models.ExpenseCategory
        fields = [
            'name',
        ]

class AccountForm(ModelForm):
    class Meta:
        model = models.Account
        fields = [
            'name',
            'number',
        ]
