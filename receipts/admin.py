from django.contrib import admin
from receipts import models

# Register your models here.
@admin.register(models.ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'owner',
    ]

@admin.register(models.Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'number',
        'owner',
    ]

@admin.register(models.Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = [
        'vendor',
        'total',
        'tax',
        'date',
        'purchaser',
        'category',
        'account',
    ]
