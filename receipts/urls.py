from django.urls import path
from receipts import views

urlpatterns = [
    path('receipts/', views.home, name='home'),
    path('receipts/create/', views.create_receipt, name='create_receipt'),
    path('receipts/categories/', views.category_list, name='category_list'),
    path('receipts/accounts/', views.account_list, name='account_list'),
    path('receipts/categories/create/', views.create_category, name='create_category'),
    path('receipts/accounts/create/', views.create_account, name='create_account'),
]
